import { Component, OnInit } from '@angular/core';
import { ApiService} from '../api.service';
import { match } from '../data/match.module';

@Component({
  selector: 'app-todaymatch',
  templateUrl: './todaymatch.component.html',
  styleUrls: ['./todaymatch.component.css']
})
export class TodaymatchComponent implements OnInit {

  m : match[];
  selectmatch:match;

  constructor(private apiService:ApiService) { }

  ngOnInit() {
    this.getdata();
    
  }

  getdata(){
    this.apiService.getmatch().subscribe(match=>{
      this.m = match;
      console.log(this.m);
    });
  }
  
}
