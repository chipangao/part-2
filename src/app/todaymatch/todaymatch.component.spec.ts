import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodaymatchComponent } from './todaymatch.component';

describe('TodaymatchComponent', () => {
  let component: TodaymatchComponent;
  let fixture: ComponentFixture<TodaymatchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodaymatchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodaymatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
