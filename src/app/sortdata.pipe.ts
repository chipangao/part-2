import { Pipe, PipeTransform } from '@angular/core';
import { match } from './data/match.module';

@Pipe({
  name: 'sortdata'
  
})
export class SortdataPipe implements PipeTransform {

  transform(getCompetition):  match[]{
    if(!getCompetition){
      return getCompetition;
    }
    return getCompetition.sort((a,b)=>{
    let dataA = Number(new Date(a.utcDate));
    let dataB = Number(new Date(b.utcDate));
    return dataB<dataA ?-1:(dataB>dataA?1:0)
  });
  }

}
