import { Component, OnInit } from '@angular/core';
import { match } from '../data/match.module';
import { ApiService } from '../api.service';


@Component({
  selector: 'app-match-board',
  templateUrl: './match-board.component.html',
  styleUrls: ['./match-board.component.css']
})
export class MatchBoardComponent implements OnInit {

  match:match[] = [];

  constructor(private apiservice:ApiService) { }

  ngOnInit() {
    this.getmatches();
  }

  getmatches():void{
    this.apiservice.getmatch().subscribe(match=>{
      this.match = match;
      console.log(this.match);
    });
  }
}
