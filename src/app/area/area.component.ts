import { Component, OnInit } from '@angular/core';
import { Area } from '../data/Areas.module';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.css']
})
export class AreaComponent implements OnInit {

  a : Area[];

  constructor(private apiService:ApiService) { }
  
  ngOnInit() {
    this.getdata();
  }

  
  getdata(){
    this.apiService.getAreas().subscribe(area=>{
      this.a = area;
      console.log(area);
    });
  }
}
