import { Pipe, PipeTransform } from '@angular/core';
import { match } from './data/match.module';


@Pipe({
  name: 'getdataId',
  pure:false
})
export class GetdataIdPipe implements PipeTransform {

    
  transform(getdata:match[],id:String): match[]{
    if(!getdata){
      return getdata;
    }
     

    return getdata.filter(getdata=>(   ( "" + getdata.id).indexOf(id.toString())!==-1));
  }

}
