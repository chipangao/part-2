import { Component, OnInit } from '@angular/core';
import { player } from '../data/players.module';
import { ApiService} from '../api.service';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {

  p:player[];

  constructor(private apiService:ApiService) { }

  ngOnInit() {
    this.getdata();
  }
   getdata(){
    this.apiService.getplayer().subscribe(player=>{
      this.p = player;
      console.log(this.p);
  });
}
}