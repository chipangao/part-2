import { Component, OnInit, Input } from '@angular/core';
import { team } from '../data/team.module';
import { ApiService } from '../api.service';
import { ActivatedRoute } from '@angular/router';
import {Location} from '@angular/common';
import { match } from '../data/match.module';

@Component({
  selector: 'app-team-detail',
  templateUrl: './team-detail.component.html',
  styleUrls: ['./team-detail.component.css']
})
export class TeamDetailComponent implements OnInit {
  @Input()
  Team:team;
  teamid:String;
  t:team[];
  Match:match;
  m:match[];
  

  constructor(private apiService:ApiService,
                private route:ActivatedRoute,
                private location:Location) { }

  ngOnInit() {
    this.getteamid();
    this.getdata();
    this.getmatchdata();
  }
  getdata(){
    this.apiService.getteam().subscribe(team=>{
      this.t = team;
      console.log(this.t);
    });
  }

  getmatchdata(){
    this.apiService.getmatch().subscribe(match=>{
      this.m = match;
      console.log(this.m);
    });
  }
  getteamid(){
    this.teamid = this.route.snapshot.paramMap.get('id');
    console.log(this.teamid);
  }

  goBack():void{
    this.location.back();
  }
}
