import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TodaymatchComponent } from './todaymatch/todaymatch.component';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from './api.service';
import { CompetitionComponent } from './competition/competition.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { Router, RouterModule } from '@angular/router';
import { CountryNameComponent } from './country-name/country-name.component';
import { AreaComponent } from './area/area.component';
import { SortdataPipe } from './sortdata.pipe';
import { FilterPipe } from './filter.pipe';
import { StandingComponent } from './standing/standing.component';
import { SortTeamPipe } from './sort-team.pipe';
import { PlayerComponent } from './player/player.component';
import { MatchBoardComponent } from './match-board/match-board.component';
import { MatchDetailComponent } from './match-detail/match-detail.component';
import { GetdataIdPipe } from './getdata-id.pipe';
import { TeamDetailComponent } from './team-detail/team-detail.component';
import { GetteamIdPipe } from './getteam-id.pipe';

@NgModule({
  declarations: [
    AppComponent,
    TodaymatchComponent,
    CompetitionComponent,
    TopBarComponent,
    CountryNameComponent,
    AreaComponent,
    SortdataPipe,
    FilterPipe,
    StandingComponent,
    SortTeamPipe,
    PlayerComponent,
    MatchBoardComponent,
    MatchDetailComponent,
    GetdataIdPipe,
    TeamDetailComponent,
    GetteamIdPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot([])
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
