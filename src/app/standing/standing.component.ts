import { Component, OnInit } from '@angular/core';
import { standings, table } from '../data/standings.module';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-standing',
  templateUrl: './standing.component.html',
  styleUrls: ['./standing.component.css']
})
export class StandingComponent implements OnInit {

  s : standings[];


  constructor(private apiService:ApiService) { }

  ngOnInit() {
    this.getdata();
  }

  getdata(){
    this.apiService.getStanding().subscribe(standing=>{
      this.s = standing;

      console.log(this.s);
    });
  }
}
