export class Competition{
    matches:[]
}

export class matches{
    id:number;
    season:[];
    utcDate:Date;
    status:String;
    sttendance:number;
    matchday:number;
    stage:String;
    group:String;
    homeTeam:[];
    awayTeam:[];
    score:[];
    goals:[];
}
export class season{
    id:number;
    startDate:Date;
    endDate:Date;
    currentMatchday:number;
}
export class homeTeam{
    id:number;
    name:String;
    coach:[];
    captain:[];
    lineup:[];
    bench:[];
}
export class coach{
    id:number;
    name:String;
    countryOfBirth:String;
    natiomality:String;
}
export class captain{
    id:number;
    name:String;
    shirNumber:number;
}
export class lineup{
    id:number;
    name:String;
    position:String;
    shirtNumber:number;
}
export class bench{
    id:number;
    name:String;
    position:String;
    shirtNumber:number;
}
export class awayTeam{
    id:number;
    name:String;
    coach:[];
    captain:[];
    lineup:[];
    bench:[];
}
export class score{
    winner:String;
    duration:String;
    fullTime:[];
    halftime:[];
    extraTime:[];
    penalties:[];
}
export class fullTime{
    homeTeam:number;
    awayTeam:number;
}
export class halftime{
    homeTeam:number;
    awayTeam:number;
}
export class extraTime{
    homeTeam:number;
    awayTeam:number;
}
export class penalties{
    homeTeam:number;
    awayTeam:number;
}
export class goals{
    minute:number;
    type:String;
    scorer:[];
    assist:[];
    bookings:[];
}
export class scorer{
    id:number;
    name:String;
}
export class assist{
    id:number;
    name:String;
}
export class bookings{
    minute:number;
    player:[];
    card:String;
}
export class player{
    id:number;
    name:String;
}
