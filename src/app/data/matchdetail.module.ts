export class matchdetail{
    id:number;
    utcDate:Date;
    status:string;
    filters:[];
    HomeTeam:[];
    awayTeam:[];
}
export class HomeTeam{
    id:number;
    name:string;
    coach:coach[];
    captain:captain[];
    lineup:lineup[];
}
export class awayTeam{
    id:number;
    name:string;
    coach:coach[];
    captain:captain[];
}   
export class coach{
    id:number;
    name:string;
    countryOfBirth: string;
    nationlity: string;
}
export class captain{
    id:number;
    name:string;
    shirtNumber:number;
}
export class lineup{
    id:number;
    name:string;
    position:string;
    shirtNumber:number;
}
export class filters{
    dateFrom: Date;
      dateTo: Date;
}