import { Component, OnInit } from '@angular/core';
import { Competition } from '../data/Allcompetition.module';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-competition',
  templateUrl: './competition.component.html',
  styleUrls: ['./competition.component.css']
})
export class CompetitionComponent implements OnInit {

  c : Competition[];
  Matchdone = true;



  constructor(private apiService:ApiService) {
   }

  ngOnInit() {
    this.getdata();

  }

  getdata(){
    this.apiService.getcompetition().subscribe(Competition=>{
      this.c = Competition;
      console.log(this.c);
    }); 
  }

}
