import { Pipe, PipeTransform } from '@angular/core';
import { standings } from './data/standings.module';

@Pipe({
  name: 'sortTeam',
  pure:false
})
export class SortTeamPipe implements PipeTransform {

  transform(getteam:standings[],type:String): standings[]{
    if(!getteam){
      return getteam;
    }
    return getteam.filter(getteam=>(getteam.type.toLowerCase().indexOf(type.toLowerCase())!==-1));
  }

}
