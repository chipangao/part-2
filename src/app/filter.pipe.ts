import { Pipe, PipeTransform } from '@angular/core';
import { match } from './data/match.module';
@Pipe({
  name: 'filter',
  pure: false
})
export class FilterPipe implements PipeTransform {

  transform(getscore:match[], status:String):match[]{
    if(!getscore){
      return getscore;
    }
    return getscore.filter(getscore=>(getscore.status.toLowerCase().indexOf(status.toLowerCase())!==-1));
  } 
}