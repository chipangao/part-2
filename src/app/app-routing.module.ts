import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MatchBoardComponent } from './match-board/match-board.component';
import { TodaymatchComponent } from './todaymatch/todaymatch.component';
import { StandingComponent } from './standing/standing.component';
import { PlayerComponent } from './player/player.component';
import { MatchDetailComponent } from './match-detail/match-detail.component';
import { TeamDetailComponent } from './team-detail/team-detail.component';

const routes: Routes = [
  {path:'matchboard', component:MatchBoardComponent},
  {path:'todaymatch',component:TodaymatchComponent},
  {path:'standing',component:StandingComponent},
  {path:'player',component:PlayerComponent},
  {path:'matchdetail/:id',component:MatchDetailComponent},
  {path:'teamdetail/:id',component:TeamDetailComponent},
  {path:'',redirectTo:'/todaymatch',pathMatch:'full'}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
